FROM node:17-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY server.js .

#For docker-compose
#ENV FRONTEND_SERVICE="frontend_app_1"
#ENV SHOPPING_CART_SERVICE="shopping_cart_app_1"

#For kubernetes
ENV FRONTEND_SERVICE="frontend"
ENV SHOPPING_CART_SERVICE="shopping_cart"

EXPOSE 3001
CMD ["npm", "start"]
